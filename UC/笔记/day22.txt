项目： 分布式流媒体组播系统
			 基于UDP的流媒体组播系统
			 
			 流媒体：在播放的过程中, 可随时截断的  mp3 mp4
			 
			 主要的项目功能类似于网络电台, 服务端并发发送多个频道的数据到组播地址 
			 客户端实现接受频道列表 并选择对应的频道数据, 使用播放器进行播放 
			 

项目分析(模块划分)：
			server:
			1. 模块1--媒体库模块, 媒体库对应的音频存储在磁盘中, 假设有一个目录
				media_lib, 一个频道对应一个子目录 
				
				例如有流行音乐频道, 有交通广播频道......
				media_lib/music 
				media_lib/traffic 
				......
				
				对于每一个频道有目录结构的要求: 必须有频道的描述和频道的数据
				
				例如：media/music/
							media/music/descr.txt  "流行音乐频道, 80 90 00后喜欢的音乐"
							media/music/benxiaohai.mp3 
							media/music/lianxi.mp3
							media/music/zhiyinnitaimei.mp3 
							......
						
							media/traffic
							media/traffic/descr.txt
							media/traffic/北清路.mp3
							media/traffic/黑龙泉路.mp3 
							......
							
			2. 模块2, 发送频道列表的线程(进程) 模块, 周期性发送频道列表
			3. 模块3, 发送各个频道的数据 （进程池/线程池）			
			4. 模块4, main 负责调度整个server的逻辑, 负责创建线程发送频道列表 
				 创建线程（进程发送频道数据） 初识化套接字等 
				 
			getline/fgets 
			
			client: 
			1. 接受频道列表
			2. 选择一个要收听的频道
			3. 父进程从组播中接受频道的数据发送给子进程, 子进程中运行播放器进行播放
			4. 父进程和子进程间的通信 pipe ...
				 父进程写管道, 子进程读管道
			5. 使用重定向 dup (pfd[0], 0);
			 