/********************************************************************************
** Form generated from reading UI file 'netserverwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NETSERVERWIDGET_H
#define UI_NETSERVERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_netServerWidget
{
public:

    void setupUi(QWidget *netServerWidget)
    {
        if (netServerWidget->objectName().isEmpty())
            netServerWidget->setObjectName(QStringLiteral("netServerWidget"));
        netServerWidget->resize(400, 300);

        retranslateUi(netServerWidget);

        QMetaObject::connectSlotsByName(netServerWidget);
    } // setupUi

    void retranslateUi(QWidget *netServerWidget)
    {
        netServerWidget->setWindowTitle(QApplication::translate("netServerWidget", "netServerWidget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class netServerWidget: public Ui_netServerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NETSERVERWIDGET_H
