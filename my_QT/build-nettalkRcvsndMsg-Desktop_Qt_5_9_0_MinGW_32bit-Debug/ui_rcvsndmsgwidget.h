/********************************************************************************
** Form generated from reading UI file 'rcvsndmsgwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RCVSNDMSGWIDGET_H
#define UI_RCVSNDMSGWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RcvsndMsgWidget
{
public:

    void setupUi(QWidget *RcvsndMsgWidget)
    {
        if (RcvsndMsgWidget->objectName().isEmpty())
            RcvsndMsgWidget->setObjectName(QStringLiteral("RcvsndMsgWidget"));
        RcvsndMsgWidget->resize(400, 300);

        retranslateUi(RcvsndMsgWidget);

        QMetaObject::connectSlotsByName(RcvsndMsgWidget);
    } // setupUi

    void retranslateUi(QWidget *RcvsndMsgWidget)
    {
        RcvsndMsgWidget->setWindowTitle(QApplication::translate("RcvsndMsgWidget", "RcvsndMsgWidget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class RcvsndMsgWidget: public Ui_RcvsndMsgWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RCVSNDMSGWIDGET_H
