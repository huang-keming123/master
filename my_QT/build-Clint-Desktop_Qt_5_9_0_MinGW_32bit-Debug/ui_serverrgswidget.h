/********************************************************************************
** Form generated from reading UI file 'serverrgswidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVERRGSWIDGET_H
#define UI_SERVERRGSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ServerRgsWidget
{
public:

    void setupUi(QWidget *ServerRgsWidget)
    {
        if (ServerRgsWidget->objectName().isEmpty())
            ServerRgsWidget->setObjectName(QStringLiteral("ServerRgsWidget"));
        ServerRgsWidget->resize(400, 300);

        retranslateUi(ServerRgsWidget);

        QMetaObject::connectSlotsByName(ServerRgsWidget);
    } // setupUi

    void retranslateUi(QWidget *ServerRgsWidget)
    {
        ServerRgsWidget->setWindowTitle(QApplication::translate("ServerRgsWidget", "ServerRgsWidget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ServerRgsWidget: public Ui_ServerRgsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVERRGSWIDGET_H
