/********************************************************************************
** Form generated from reading UI file 'calwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALWIDGET_H
#define UI_CALWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CalWidget
{
public:

    void setupUi(QWidget *CalWidget)
    {
        if (CalWidget->objectName().isEmpty())
            CalWidget->setObjectName(QStringLiteral("CalWidget"));
        CalWidget->resize(400, 300);

        retranslateUi(CalWidget);

        QMetaObject::connectSlotsByName(CalWidget);
    } // setupUi

    void retranslateUi(QWidget *CalWidget)
    {
        CalWidget->setWindowTitle(QApplication::translate("CalWidget", "CalWidget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CalWidget: public Ui_CalWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALWIDGET_H
