#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QUdpSocket>
#include <QCryptographicHash>
#include "proto.h"
#include <QDateTime>
namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void sdReadReadySlot();
    void on_pushButton_3_clicked(bool checked);
    void on_pushButton_2_clicked(bool checked);

    void on_pushButton_3_clicked();

private:
    void showList();

private:
    Ui::MainWindow *ui;

    QUdpSocket *sd;
    con_t List[5];
};

#endif // MAINWINDOW_H
