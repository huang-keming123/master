#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QPixmap>
#include <QPainter>
#include <QLabel>
#include <QCursor>
#include <QUdpSocket>
#include "proto.h"
#include <QString>

namespace Ui {
class LoginWidget;
}

class LoginWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LoginWidget(QWidget *parent = 0);
    ~LoginWidget();
private slots:
    void rgsBtnSlot ();
    void loginBtnSlot ();
    void loginSdReadyReadSlot();
private:
    Ui::LoginWidget *ui;

    QLabel *label;
    QLabel *loginLabel;
    QLabel *pwdLabel;

    QLineEdit *cntLineEdit;
    QLineEdit *pwdLineEdit;
    QCheckBox *autoLoginBox;
    QCheckBox *rmPwdBox;
    QPushButton *findPwdBtn;
    QPushButton *loginBtn;
    QPushButton *rgsBtn;
    QPushButton *imgBtn;

    QHBoxLayout *lableLayout;
    QHBoxLayout *checkBoxLayout;
    QHBoxLayout *loginLayout;
    QHBoxLayout *pwdLayout;
    QVBoxLayout *mainlayout;

    QUdpSocket *loginSd;
};

#endif // LOGINWIDGET_H
