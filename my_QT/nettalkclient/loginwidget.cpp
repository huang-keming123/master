#include "loginwidget.h"
#include "ui_loginwidget.h"
#include "rgswidget.h"
#include <mainwindow.h>
LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWidget)
{
    ui->setupUi(this);

    //window
    this->setMinimumSize(540, 400);
    this->setMaximumSize(540, 400);
    this->setWindowTitle("QQ");

    //窗口无边框化
    //this->setWindowFlag(Qt::FramelessWindowHint);

    //设置qq icon
    QIcon windowIcon(QString (":/new/prefix1/mypic/qqicon_top.png"));
    this->setWindowIcon(windowIcon);

    //bg
    QPixmap pm (QString(":/new/prefix1/mypic/new_qqbackgroud.jpg"));
    pm.scaled(this->width(), this->height());
    QBrush br(pm);
    QPalette pl;
    pl.setBrush(QPalette::Background, br);
    this->setPalette(pl);

     //左上角图标显示
//    QPixmap pixmap(QString (":/new/prefix1/mypic/qqicon_top.png"));
//    QLabel* lab = new QLabel(this);
//    lab->setPixmap(pixmap.scaled(50, 50));

    //头像
    QPixmap pixmapHead (QString (":/new/prefix1/mypic/qqhead.png"));
    label = new QLabel(this);
    label->setPixmap(pixmapHead.scaled(100, 100));

    //文本框前文字
    loginLabel = new QLabel("账号");
    pwdLabel = new QLabel("密码");


    //用户名和密码
    cntLineEdit = new QLineEdit (this);
    pwdLineEdit = new QLineEdit (this);
    //设置文本框提示字
    cntLineEdit->setPlaceholderText("QQ号码/手机/邮箱");
    pwdLineEdit->setPlaceholderText("密码");
    //设置密码文
    pwdLineEdit->setEchoMode(QLineEdit::Password);

    //设置大小和位置
    cntLineEdit->setFixedSize(260, 30);
    pwdLineEdit->setFixedSize(260, 30);

    cntLineEdit->setStyleSheet("border-color:#000000;border-style:solid;border-top-width:0px;border-right-width:0px;border-bottom-width:1px;border-left-width:0px;");
    pwdLineEdit->setStyleSheet("border-color:#000000;border-style:solid;border-top-width:0px;border-right-width:0px;border-bottom-width:1px;border-left-width:0px;");
    //复选框
    autoLoginBox = new QCheckBox(QString ("自动登录"));
    rmPwdBox = new QCheckBox(QString ("记住密码"));

    autoLoginBox->setStyleSheet("color: rgb(186,186,186);");
    rmPwdBox->setStyleSheet("color: rgb(186,186,186);");
    //找回密码按钮
    findPwdBtn = new QPushButton;
    findPwdBtn->setParent(this);
    findPwdBtn->setText(QString ("找回密码"));
    findPwdBtn->setFlat(true);
    findPwdBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    findPwdBtn->setStyleSheet("QPushButton{color: rgb(186,186,186);border: 0px;}QPushButton::hover{color: #959595;}");

    //登录按钮
    loginBtn = new QPushButton;
    //设置按钮颜色
    loginBtn->setParent(this);
    loginBtn->setText(QString ("登录"));
    loginBtn->setStyleSheet("QPushButton{font-size: 20px;color:white; background-color:rgb(31,201,253);border-radius: 10px;}QPushButton::hover{background-color: #1cb5e4;}");

    loginBtn->setFixedSize(300, 50);
    loginBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    connect(loginBtn, SIGNAL(clicked(bool)), this, SLOT(loginBtnSlot()));

    //横向布局
    checkBoxLayout = new QHBoxLayout;
    checkBoxLayout->addStretch();
    checkBoxLayout->addWidget(autoLoginBox);
    checkBoxLayout->addStretch();
    checkBoxLayout->addWidget(rmPwdBox);
    checkBoxLayout->addStretch();
    checkBoxLayout->addWidget(findPwdBtn);
    checkBoxLayout->addStretch();

    lableLayout = new QHBoxLayout;
    lableLayout->setAlignment(Qt::AlignCenter);
    lableLayout->addWidget(label);

    loginLayout = new QHBoxLayout;
    loginLayout->addWidget(loginLabel);
    loginLayout->addWidget(cntLineEdit);

    pwdLayout = new QHBoxLayout;
    pwdLayout->addWidget(pwdLabel);
    pwdLayout->addWidget(pwdLineEdit);

    //纵向布局
    mainlayout = new QVBoxLayout;
    mainlayout->setAlignment(Qt::AlignCenter);
    mainlayout->addLayout(lableLayout);
    mainlayout->addLayout(loginLayout);
    mainlayout->addLayout(pwdLayout);
    mainlayout->addLayout(checkBoxLayout);
    mainlayout->addWidget(loginBtn);
    mainlayout->setContentsMargins(120, 90, 120, 20);
    this->setLayout(mainlayout);

    //注册按钮
    rgsBtn = new QPushButton;
    rgsBtn->setParent(this);
    rgsBtn->setText(QString ("注册账号"));
    rgsBtn->setFlat(true);
    rgsBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    rgsBtn->move(10, 350);
    rgsBtn->setFixedSize(80, 50);
    rgsBtn->setStyleSheet("QPushButton{color: rgb(186,186,186);border: 0px;}QPushButton::hover{color: #959595;}");

    connect(rgsBtn, SIGNAL(clicked(bool)), this, SLOT(rgsBtnSlot()));
    //rgsBtn->setStyleSheet("border: 0px"); //消除边框
    //二维码
    imgBtn = new QPushButton;
    imgBtn->setParent(this);
    QPixmap imgPixmap (QString (":/new/prefix1/mypic/image.jpg"));
    QPixmap fitpixmap = imgPixmap.scaled(30, 30, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    imgBtn->setIcon(QIcon(fitpixmap));
    imgBtn->setFixedSize(30, 30);
    imgBtn->move(489, 360);
    imgBtn->setFlat(true);
    //imgBtn->setStyleSheet("border: 0px"); //消除边框
    imgBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    loginSd = new QUdpSocket;
    connect(loginSd, SIGNAL(readyRead()), this, SLOT(loginSdReadyReadSlot()));
}

LoginWidget::~LoginWidget()
{
    delete ui;
}

void LoginWidget::rgsBtnSlot()
{
    rgswidget *rgs = new rgswidget;
    rgs->show();
}

void LoginWidget::loginBtnSlot()
{
    rgs_t sndbuf;
    QString cntstr = cntLineEdit->text();
    QString pwdstr = pwdLineEdit->text();

    strncpy (sndbuf.cnt, cntstr.toLatin1().data(), CNTSIZE);
    QByteArray pwdarr = QCryptographicHash::hash(pwdstr.toLatin1(), QCryptographicHash::Sha3_512);
    strncpy (sndbuf.pwd, pwdarr.toHex().data(), PWDSIZE);
    loginSd->writeDatagram((char*)&sndbuf, sizeof(sndbuf), QHostAddress(RGS_SERVER_IP), RGS_SERVER_PORT);
}

void LoginWidget::loginSdReadyReadSlot()
{
    login_t rcvbuf;
    rcvbuf.login_state = Login_ERROR;
    int ret;
    loginSd->readDatagram((char*)&rcvbuf, sizeof(rcvbuf));
    qDebug() << rcvbuf.login_state;
    MainWindow *mm;

    switch (rcvbuf.login_state)
    {
        case Login_OK:
            //QMessageBox::information(this, QString("登录提示"), QString("恭喜你,登录成功！"),QMessageBox::Ok);
            mm = new MainWindow();
            this->close();
            mm->show();
            break;
        case Login_ERROR:
            ret = QMessageBox::question(this, QString("登录提示"),QString("账号或密码错误,是否重新登录？"),QMessageBox::Ok,QMessageBox::No);
            if (ret == QMessageBox::Ok)
            {
                cntLineEdit->clear();
                pwdLineEdit->clear();
            }
            else
            {
                this->close();
            }
        default:
            break;
    }
}
