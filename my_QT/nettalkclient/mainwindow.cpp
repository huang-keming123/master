#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "loginwidget.h"
int i = 0;
MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sd = new QUdpSocket();
    connect(sd, SIGNAL(readyRead()), this, SLOT(sdReadReadySlot()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    con_t sndbuf;
    memset(List, 0, sizeof(List));
    //读取当前昵称
    QString cntstr = ui->lineEdit->text();
    QString pwdstr;

    strncpy (sndbuf.cnt, cntstr.toLatin1().data(), CNTSIZE);

    //发送正在连接的用户名
    sd->writeDatagram((char*)&sndbuf, sizeof(sndbuf), QHostAddress(SERVER_IP), SERVER_PORT);
}

//void MainWindow::sdReadReadySlot()
//{
//    //接收返回的用户名列表
//    //sd->readDatagram((char*)List, sizeof(List));
//}

void MainWindow::sdReadReadySlot()
{
    char rcvbuf[1024] = {0};
    QDateTime currentTime = QDateTime::currentDateTime();
    QString curtime = currentTime.toString("yyyy-MM-dd hh:mm:ss");
    //接收服务器发送来的信息
    sd->readDatagram(rcvbuf, 1024);
    QString rcv(rcvbuf);
    curtime.append('\n');
    curtime += rcv;
    curtime.append('\n');
    qDebug() << curtime;
    //显示到聊天界面
    ui->textEdit_2->append(curtime);
}


void MainWindow::on_pushButton_2_clicked(bool checked)
{
    //读取准备发送的消息
    QString msgbuf;
    msgbuf = ui->lineEdit->text();
    msgbuf += ":";
    msgbuf += ui->lineEdit_4->text();
    msgbuf += "\0";
    const char* msg = msgbuf.toStdString().data();
    //strcpy(msg, msgbuf.toLatin1().data());
    ui->lineEdit_4->clear();
    //通过socket发送到服务器
    sd->writeDatagram(msg, strlen(msg), QHostAddress(SERVER_IP), SERVER_PORT);
}
