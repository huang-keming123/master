#ifndef NETTALKSERVER_H
#define NETTALKSERVER_H

#include <QWidget>

namespace Ui {
class nettalkServer;
}

class nettalkServer : public QWidget
{
    Q_OBJECT

public:
    explicit nettalkServer(QWidget *parent = 0);
    ~nettalkServer();

private:
    Ui::nettalkServer *ui;
};

#endif // NETTALKSERVER_H
