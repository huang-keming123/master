#include "rgswidget.h"
#include "ui_rgswidget.h"
#include "proto.h"

rgswidget::rgswidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::rgswidget)
{
    ui->setupUi(this);


    QIcon rgsIcon(QString (":/new/prefix1/mypic/rgsicon.png"));
    this->setWindowIcon(rgsIcon);
    this->setWindowTitle("注册");

    //bg
    QPixmap rgspm (QString(":/new/prefix1/mypic/rgsWindow.jpg"));
    rgspm.scaled(this->width(), this->height());
    QBrush br(rgspm);
    QPalette pl;
    pl.setBrush(QPalette::Background, br);
    this->setPalette(pl);

    //文本框前文字
    loginLabel = new QLabel("账号");
    pwdLabel = new QLabel("密码");

    //用户名和密码
    cntLineEdit = new QLineEdit (this);
    pwdLineEdit = new QLineEdit (this);

    //设置文本框提示字
    cntLineEdit->setPlaceholderText("QQ号码/手机/邮箱");
    pwdLineEdit->setPlaceholderText("密码");
    //设置密码文
    pwdLineEdit->setEchoMode(QLineEdit::Password);

    //设置大小和位置
    cntLineEdit->setFixedSize(260, 30);
    pwdLineEdit->setFixedSize(260, 30);

    //cntLineEdit->setStyleSheet("border-color:#000000;border-style:solid;border-top-width:0px;border-right-width:0px;border-bottom-width:1px;border-left-width:0px;");
    //pwdLineEdit->setStyleSheet("border-color:#000000;border-style:solid;border-top-width:0px;border-right-width:0px;border-bottom-width:1px;border-left-width:0px;");

    //登录按钮
    loginBtn = new QPushButton;
    //设置按钮颜色
    loginBtn->setParent(this);
    loginBtn->setText(QString ("立即注册"));
    loginBtn->setStyleSheet("QPushButton{font-size: 20px;color:white; background-color:rgb(31,201,253);border-radius: 10px;}QPushButton::hover{background-color: #1cb5e4;}");

    loginBtn->setFixedSize(300, 50);
    loginBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    //横向布局
    loginLayout = new QHBoxLayout;
    loginLayout->addWidget(loginLabel);
    loginLayout->addWidget(cntLineEdit);

    pwdLayout = new QHBoxLayout;
    pwdLayout->addWidget(pwdLabel);
    pwdLayout->addWidget(pwdLineEdit);

    //纵向布局
    mainlayout = new QVBoxLayout;
    mainlayout->setAlignment(Qt::AlignCenter);
    mainlayout->addLayout(loginLayout);
    mainlayout->addLayout(pwdLayout);
    mainlayout->addWidget(loginBtn);
    mainlayout->setContentsMargins(120, 50, 120, 20);
    this->setLayout(mainlayout);

    //socket
    sd = new QUdpSocket();

    connect (loginBtn,SIGNAL(clicked(bool)), this, SLOT(rgsPushBtnSlot()));
    connect(sd, SIGNAL(readyRead()), this, SLOT(sdReadyReadSlot()));
}

rgswidget::~rgswidget()
{
    delete ui;
}

void rgswidget::rgsPushBtnSlot()
{
    //将用户输入的信息打包发送给注册服务器
    rgs_t sndbuf;
    QString cntstr = cntLineEdit->text();
    QString pwdstr = pwdLineEdit->text();

    strncpy (sndbuf.cnt, cntstr.toLatin1().data(), CNTSIZE);
    QByteArray pwdarr = QCryptographicHash::hash(pwdstr.toLatin1(), QCryptographicHash::Sha3_512);
    strncpy (sndbuf.pwd, pwdarr.toHex().data(), PWDSIZE);
    sd->writeDatagram((char*)&sndbuf, sizeof(sndbuf), QHostAddress(RGS_SERVER_IP), RGS_SERVER_PORT);



}

void rgswidget::sdReadyReadSlot()
{
    rgs_t rcvbuf;
    int ret;
    sd->readDatagram((char*)&rcvbuf, sizeof(rcvbuf));
    switch (rcvbuf.rgs_state)
    {
        case RGS_OK:
            QMessageBox::information(this, QString("注册提示"), QString("恭喜你,注册成功！"),QMessageBox::Ok);
            this->close();
            break;
        case RGS_EXITS:
            ret = QMessageBox::question(this, QString("注册提示"),QString("此账号已存在,是否重新注册？"),QMessageBox::Ok,QMessageBox::No);
            if (ret == QMessageBox::Ok)
            {
                cntLineEdit->clear();
                pwdLineEdit->clear();
            }
            else
            {
                this->close();
            }
        case RGS_ERROR:
            break;
        default:
            break;
    }
}
