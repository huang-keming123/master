#ifndef RGSWIDGET_H
#define RGSWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QUdpSocket>
#include <QString>
#include <QCryptographicHash>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class rgswidget;
}

class rgswidget : public QWidget
{
    Q_OBJECT

public:
    explicit rgswidget(QWidget *parent = 0);
    ~rgswidget();
private slots:
    void rgsPushBtnSlot();
    void sdReadyReadSlot();
private:
    Ui::rgswidget *ui;
    QLabel *loginLabel;
    QLabel *pwdLabel;

    QLineEdit *cntLineEdit;
    QLineEdit *pwdLineEdit;
    QPushButton *loginBtn;

    QHBoxLayout *loginLayout;
    QHBoxLayout *pwdLayout;
    QVBoxLayout *mainlayout;

    QUdpSocket *sd;
};

#endif // RGSWIDGET_H
