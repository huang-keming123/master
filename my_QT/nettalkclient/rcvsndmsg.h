#ifndef RCVSNDMSG_H
#define RCVSNDMSG_H

#include <QWidget>

namespace Ui {
class RcvsndMsg;
}

class RcvsndMsg : public QWidget
{
    Q_OBJECT

public:
    explicit RcvsndMsg(QWidget *parent = 0);
    ~RcvsndMsg();

private:
    Ui::RcvsndMsg *ui;
};

#endif // RCVSNDMSG_H
