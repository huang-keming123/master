#include "calwidget.h"
#include "ui_calwidget.h"
#include <QString>
#include <QGridLayout>

CalWidget::CalWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CalWidget)
{
    ui->setupUi(this);

    one = new QPushButton(QString("1"));
    two = new QPushButton(QString("2"));
    three = new QPushButton(QString("3"));
    zero = new QPushButton(QString("0"));
    dot = new QPushButton(QString("."));
    eql = new QPushButton(QString("="));
    plus = new QPushButton(QString("+"));
    plus->setFixedHeight(60);

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(one, 0, 0);
    layout->addWidget(two, 0, 1);
    layout->addWidget(three, 0, 2);
    layout->addWidget(zero, 1, 0);
    layout->addWidget(dot, 1, 1);
    layout->addWidget(eql, 1, 2);
    layout->addWidget(plus, 0, 3, 2, 1);

    layout->setHorizontalSpacing(20);
    layout->setVerticalSpacing(20);

    layout->setContentsMargins(100, 100, 100, 100);

    this->setLayout(layout);



}

CalWidget::~CalWidget()
{
    delete ui;
}
