#ifndef CALWIDGET_H
#define CALWIDGET_H

#include <QWidget>
#include <QPushButton>

namespace Ui {
class CalWidget;
}

class CalWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CalWidget(QWidget *parent = 0);
    ~CalWidget();

private:
    Ui::CalWidget *ui;
    QPushButton *one, *two, *three;
    QPushButton *zero, *dot, *eql;
    QPushButton *plus;
};

#endif // CALWIDGET_H
