#include "testwidget.h"
#include "ui_testwidget.h"
#include "QString"
#include <QIcon>
#include <QPalette>
#include <QBrush>
#include <QPixmap>

TestWidget::TestWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestWidget)
{
    ui->setupUi(this);

    //window
    this->resize(600, 300);
    this->setWindowTitle(QString ("A Test Window"));
    QIcon windowIcon (QString (":/new/prefix1/myicon.jpg"));
    this->setWindowIcon(windowIcon);

    //bg
    QPixmap pm (QString (":/new/prefix1/mybackgroud.jpg"));
    pm = pm.scaled (this->width(), this->height());
    QBrush br (pm);
    QPalette pl;
    pl.setBrush(QPalette::Background, br);
    this->setPalette(pl);

    //button
    btn = new QPushButton;
    btn->setParent(this);
    btn->setText(QString ("Login"));
    btn->move(150, 180);

    btn1 = new QPushButton;
    btn1->setParent(this);
    btn1->setText(QString ("Register"));
    btn1->move(370, 180);
}

TestWidget::~TestWidget()
{
    delete ui;
}
