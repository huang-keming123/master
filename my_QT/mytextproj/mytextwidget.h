#ifndef MYTEXTWIDGET_H
#define MYTEXTWIDGET_H

#include <QWidget>
#include <QCheckBox>
#include <QRadioButton>
#include <QPlainTextEdit>
#include <QButtonGroup>

namespace Ui {
class MytextWidget;
}

class MytextWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MytextWidget(QWidget *parent = 0);
    ~MytextWidget();
private slots:
    //加粗复选框的槽函数声明
    void chkBoxBoldSlot ();
    void chkBoxUnderSlot ();
    void chkBoxItalicSlot ();
    void rBtnSlots ();
private:
    Ui::MytextWidget *ui;
    QRadioButton *rbtnRed, *rbtnBlue, *rbtnBlack;
    QCheckBox *chkBoxUnder, *chkBoxItalic, *chkBoxBold;
    QPlainTextEdit *txtEdt;
    QButtonGroup *groupbtn;
};

#endif // MYTEXTWIDGET_H
