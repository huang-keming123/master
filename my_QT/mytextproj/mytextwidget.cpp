#include "mytextwidget.h"
#include "ui_mytextwidget.h"
#include <QString>
#include <QPixmap>
#include <QPalette>
#include <QIcon>
#include <QBrush>
#include <QHBoxLayout>
#include <QVBoxLayout>


MytextWidget::MytextWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MytextWidget)
{
    ui->setupUi(this);
    //windows
    this->setMaximumSize(400, 300);
    this->setMinimumSize(400, 300);
    this->setWindowTitle(QString ("Set Text"));
    QIcon windowicon = QIcon (QPixmap (QString (":/new/prefix1/icon.jpg")));
    this->setWindowIcon(windowicon);

    //bg
    QPalette pal;
    pal.setBrush(QPalette::Background, QBrush (QPixmap (QString (":/new/prefix1/mybackgroud.jpg")).scaled (400, 300)));
    this->setPalette(pal);

    //init
    chkBoxBold = new QCheckBox ("Bold");
    chkBoxUnder = new QCheckBox ("Underline");
    chkBoxItalic = new QCheckBox ("Italic");

    rbtnRed = new QRadioButton ("Red");
    rbtnBlue = new QRadioButton ("Blue");
    rbtnBlack = new QRadioButton ("Black");

    txtEdt = new QPlainTextEdit;
    txtEdt->setPlainText(QString ("Plase input!!!"));

    QFont font = txtEdt->font();
    font.setPointSize(20);
    txtEdt->setFont(font);

    //单选按钮做成单选按钮组
    groupbtn = new QButtonGroup();
    groupbtn->addButton(rbtnBlack, 0);
    groupbtn->addButton(rbtnBlue, 1);
    groupbtn->addButton(rbtnRed, 2);
    //默认选中黑色
    rbtnBlack->setChecked(true);


    //layout
    QHBoxLayout *hlay1 = new QHBoxLayout;
    hlay1->addWidget(chkBoxBold);
    hlay1->addWidget(chkBoxUnder);
    hlay1->addWidget(chkBoxItalic);

    QHBoxLayout *hlay2 = new QHBoxLayout;
    hlay2->addWidget(rbtnRed);
    hlay2->addWidget(rbtnBlue);
    hlay2->addWidget(rbtnBlack);


    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(hlay1);
    mainLayout->addLayout(hlay2);
    mainLayout->addWidget(txtEdt);

    this->setLayout(mainLayout);
 //连接信号与槽
    connect(chkBoxBold,SIGNAL(clicked(bool)),this,SLOT(chkBoxBoldSlot()));
    connect(chkBoxUnder,SIGNAL(clicked(bool)),this,SLOT(chkBoxUnderSlot()));
    connect(chkBoxItalic,SIGNAL(clicked(bool)),this,SLOT(chkBoxItalicSlot()));
    connect(rbtnBlack,SIGNAL(clicked(bool)),this,SLOT(rBtnSlots()));
    connect(rbtnBlue,SIGNAL(clicked(bool)),this,SLOT(rBtnSlots()));
    connect(rbtnRed,SIGNAL(clicked(bool)),this,SLOT(rBtnSlots()));
}

MytextWidget::~MytextWidget()
{
    delete ui;
}

void MytextWidget::chkBoxBoldSlot()
{
    QFont font = txtEdt->font();
    if (chkBoxBold->isChecked())
    {
        font.setBold(true);
    }
    else
    {
        font.setBold(false);
    }
    txtEdt->setFont(font);
}

void MytextWidget::chkBoxUnderSlot()
{
    QFont font = txtEdt->font();
    if (chkBoxUnder->isChecked())
    {
        font.setUnderline(true);
    }
    else
    {
        font.setUnderline(false);
    }
    txtEdt->setFont(font);
}

void MytextWidget::chkBoxItalicSlot()
{
    QFont font = txtEdt->font();
    if (chkBoxItalic->isChecked())
    {
        font.setItalic(true);
    }
    else
    {
        font.setItalic(false);
    }
    txtEdt->setFont(font);
}

void MytextWidget::rBtnSlots()
{
    int id = groupbtn->checkedId();
    QString colorText;
    QString curtext = txtEdt->toPlainText();
    txtEdt->clear();
    colorText.clear();
    switch (id)
    {
        case 0:
            colorText.append("<font color=\"#000000\">");
            break;
        case 1:
            colorText.append("<font color=\"#0000FF\">");
            break;
        case 2:
            colorText.append("<font color=\"#FF0000\">");
            break;
        default:
            break;
    }
    colorText.append(curtext);
    colorText.append("<font>");
    txtEdt->appendHtml(colorText);
}
