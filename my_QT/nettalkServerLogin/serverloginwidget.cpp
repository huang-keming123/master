#include "serverloginwidget.h"
#include "ui_serverloginwidget.h"
#include "proto.h"
ServerLoginWidget::ServerLoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServerLoginWidget)
{
    ui->setupUi(this);

    sd = new QUdpSocket;
    sd->bind(QHostAddress(LOGIN_SERVER_IP), LOGIN_SERVER_PORT);

    //链接信号与槽,当socket中有数据可以读的时候触发槽函数
    connect(sd, SIGNAL(readyRead()), this, SLOT(sdReadyReadSlot()));

}

ServerLoginWidget::~ServerLoginWidget()
{
    delete ui;
}

void ServerLoginWidget::sdReadyReadSlot()
{
    login_t rcvbuf;
    QHostAddress raddr;
    quint16 rport;

    sd->readDatagram((char*)&rcvbuf, sizeof(rcvbuf), &raddr, &rport);

    QSqlDatabase sql = QSqlDatabase::addDatabase("QSQLITE");
    sql.setDatabaseName("D:\\my_QT\\build-nettalkServerRgs-Desktop_Qt_5_9_0_MinGW_32bit-Debug\\debug\\iot_server.dat");
    //打开数据库
    if (!sql.open())
        qDebug() << "open Error!!!";
    else
        qDebug() << "open Successed!!!";

    //查表
    QSqlQuery sql_query;
    QString cnt;
    QString pwd;
    bool ret;
    char buf[256];
    if(sql_query.exec("select * from userTable"))
    {
        while((ret = sql_query.next()) == true)
        {
            cnt = sql_query.value(0).toString();
            if (strcmp(rcvbuf.cnt, cnt.toLatin1().data()) == 0)
            {
                pwd = sql_query.value(1).toString();
                if (strcmp(rcvbuf.pwd, pwd.toLatin1().data()) == 0)
                    rcvbuf.login_state = Login_OK;
                else
                    rcvbuf.login_state = Login_ERROR;
                break;
            }
            else
            {
                rcvbuf.login_state = Login_ERROR;
            }
        }
    }
    sd->writeDatagram((char*)&rcvbuf, sizeof (rcvbuf), raddr, rport);
}
