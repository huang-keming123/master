#ifndef PROTO_H
#define PROTO_H

//注册功能的协议部分
#define LOGIN_SERVER_IP   "10.11.7.139"
#define LOGIN_SERVER_PORT 1100
#define CNTSIZE 32
#define PWDSIZE 256

enum
{
    Login_OK,
    Login_ERROR
};

typedef struct
{
    char cnt[CNTSIZE];
    char pwd[PWDSIZE];
    int8_t login_state;
}login_t;







#endif // PROTO_H
