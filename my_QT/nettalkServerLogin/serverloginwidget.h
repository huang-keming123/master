#ifndef SERVERLOGINWIDGET_H
#define SERVERLOGINWIDGET_H

#include <QWidget>
#include <QUdpSocket>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

namespace Ui {
class ServerLoginWidget;
}

class ServerLoginWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ServerLoginWidget(QWidget *parent = 0);
    ~ServerLoginWidget();

private slots:
    void sdReadyReadSlot();

private:
    Ui::ServerLoginWidget *ui;

    QUdpSocket *sd;
};

#endif // SERVERLOGINWIDGET_H
