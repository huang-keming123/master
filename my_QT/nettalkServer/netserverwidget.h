#ifndef NETSERVERWIDGET_H
#define NETSERVERWIDGET_H

#include "proto.h"
#include <QWidget>
#include <QUdpSocket>

namespace Ui {
class netServerWidget;
}

class netServerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit netServerWidget(QWidget *parent = 0);
    ~netServerWidget();
private slots:
    void sdReadyReadSlot();

private:
    Ui::netServerWidget *ui;
    QUdpSocket *sd;
    con_t rcvbuf;
    con_t List[5];
    QHostAddress raddr;
    quint16 rport;
    QHostAddress raddrList[10];
    quint16 rportList[10];

};

#endif // NETSERVERWIDGET_H
