/********************************************************************************
** Form generated from reading UI file 'rgswidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RGSWIDGET_H
#define UI_RGSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_rgswidget
{
public:

    void setupUi(QWidget *rgswidget)
    {
        if (rgswidget->objectName().isEmpty())
            rgswidget->setObjectName(QStringLiteral("rgswidget"));
        rgswidget->resize(400, 300);

        retranslateUi(rgswidget);

        QMetaObject::connectSlotsByName(rgswidget);
    } // setupUi

    void retranslateUi(QWidget *rgswidget)
    {
        rgswidget->setWindowTitle(QApplication::translate("rgswidget", "Form", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class rgswidget: public Ui_rgswidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RGSWIDGET_H
