/********************************************************************************
** Form generated from reading UI file 'easthomewidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EASTHOMEWIDGET_H
#define UI_EASTHOMEWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EastHomeWidget
{
public:

    void setupUi(QWidget *EastHomeWidget)
    {
        if (EastHomeWidget->objectName().isEmpty())
            EastHomeWidget->setObjectName(QStringLiteral("EastHomeWidget"));
        EastHomeWidget->resize(400, 300);

        retranslateUi(EastHomeWidget);

        QMetaObject::connectSlotsByName(EastHomeWidget);
    } // setupUi

    void retranslateUi(QWidget *EastHomeWidget)
    {
        EastHomeWidget->setWindowTitle(QApplication::translate("EastHomeWidget", "EastHomeWidget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class EastHomeWidget: public Ui_EastHomeWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EASTHOMEWIDGET_H
