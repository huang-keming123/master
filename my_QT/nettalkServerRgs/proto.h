#ifndef PROTO_H
#define PROTO_H

//注册功能的协议部分
#define RGS_SERVER_IP   "10.11.7.139"
#define RGS_SERVER_PORT 1100
#define CNTSIZE 32
#define PWDSIZE 256

enum
{
    RGS_OK,
    RGS_EXITS,
    RGS_ERROR
};

typedef struct
{
    char cnt[CNTSIZE];
    char pwd[PWDSIZE];
    int8_t rgs_state;
}rgs_t;







#endif // PROTO_H
