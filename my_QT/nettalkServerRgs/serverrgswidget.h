#ifndef SERVERRGSWIDGET_H
#define SERVERRGSWIDGET_H

#include <QWidget>
#include <QUdpSocket>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

namespace Ui {
class ServerRgsWidget;
}

class ServerRgsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ServerRgsWidget(QWidget *parent = 0);
    ~ServerRgsWidget();

private slots:
    void sdReadyReadSlot();

private:
    Ui::ServerRgsWidget *ui;

    QUdpSocket *sd;
};

#endif // SERVERRGSWIDGET_H
