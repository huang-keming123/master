#include "serverrgswidget.h"
#include "ui_serverrgswidget.h"
#include "proto.h"

ServerRgsWidget::ServerRgsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServerRgsWidget)
{
    ui->setupUi(this);

    sd = new QUdpSocket;
    sd->bind(QHostAddress(RGS_SERVER_IP), RGS_SERVER_PORT);

    //链接信号与槽,当socket中有数据可以读的时候触发槽函数
    connect(sd, SIGNAL(readyRead()), this, SLOT(sdReadyReadSlot()));
}

ServerRgsWidget::~ServerRgsWidget()
{
    delete ui;
}

void ServerRgsWidget::sdReadyReadSlot()
{
    //客户端发来请求
    //接收客户端发来的结构体去数据库中验证是否可以注册并将验证结果发回给客户端
    rgs_t rcvbuf;
    QHostAddress raddr;
    quint16 rport;


    sd->readDatagram((char*)&rcvbuf, sizeof(rcvbuf), &raddr, &rport);

    //sqlit3
    //建立数据库
    QSqlDatabase sql = QSqlDatabase::addDatabase("QSQLITE");
    sql.setDatabaseName("D:\\my_QT\\build-nettalkServerRgs-Desktop_Qt_5_9_0_MinGW_32bit-Debug\\debug\\iot_server.dat");
    //打开数据库
    if (!sql.open())
        qDebug() << "open Error!!!";
    else
        qDebug() << "open Successed!!!";
    //建立数据表-注册表
    QSqlQuery sql_query;
    if (!sql_query.exec("create table if not exists userTable(count text primary key, password text not null)"))
        qDebug() << "create Error！";
    else
        qDebug() << "create Successed!";

    //查表
    QString cnt;
    bool ret;
    char buf[256];
    if(sql_query.exec("select * from userTable"))
    {
        while((ret = sql_query.next()) == true)
        {
            cnt = sql_query.value(0).toString();
            if (strcmp(rcvbuf.cnt, cnt.toLatin1().data()) == 0)
            {
                rcvbuf.rgs_state = RGS_EXITS;
                break;
            }
        }
    }
    if (!ret)
    {
        rcvbuf.rgs_state = RGS_OK;
        memset (buf,'\0',256);
        snprintf(buf, 256, "insert into userTable values(\"%s\", \"%s\")", rcvbuf.cnt, rcvbuf.pwd);
        if (!sql_query.exec(QString::fromLocal8Bit(buf)))
            qDebug() << "insert Error!";
        else
            qDebug() << "insert Successed!";
    }


    sd->writeDatagram((char*)&rcvbuf, sizeof (rcvbuf), raddr, rport);
}
