#ifndef EASTHOMEWIDGET_H
#define EASTHOMEWIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QBrush>
#include <QPalette>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStackedLayout>
#include <QLayout>
#include <QGridLayout>

namespace Ui {
class EastHomeWidget;
}

class EastHomeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EastHomeWidget(QWidget *parent = 0);
    ~EastHomeWidget();
private slots:
    void loginBtnSlot();
    void zhuxiaoPushSlot();
    void ktBtnSlot();
    void ktRetBtnSlot();

private:
    //初始化Login界面
    void initLogin ();
    //初始化按钮
    void initNineBtn();
    //显示九宫格界面
    void nineShow();
    //关闭九宫格界面
    void nineClose();
    //初始化客厅界面
    void ktInit();
private:
    Ui::EastHomeWidget *ui;
    QLabel *LoginBg;
    QLabel *cntBg;
    QLabel *pwdBg;
    QLabel *whiteBg;
    QLabel *centerPic;
    QLabel *kongtiaozuoPic;
    QLabel *kongtiaoyouPic;
    QLabel *jianKongPic;

    QLineEdit *LoginEdit;
    QLineEdit *PwdEdit;

    QPushButton *LoginBtn;
    QPushButton *ktBtn;
    QPushButton *ws1Btn;
    QPushButton *ws2Btn;
    QPushButton *ws3Btn;
    QPushButton *ckBtn;
    QPushButton *dxsBtn;
    QPushButton *cfBtn;
    QPushButton *wjBtn;
    QPushButton *wdmBtn;
    QPushButton *zhuxiaoPush;
    QPushButton *clROnBtn;
    QPushButton *clROffBtn;
    QPushButton *clLOnBtn;
    QPushButton *clLOffBtn;
    QPushButton *bjqOnBtn;
    QPushButton *Light1Btn;
    QPushButton *Light2Btn;
    QPushButton *Light3Btn;
    QPushButton *Light4Btn;
    QPushButton *bjqOffBtn;
    QPushButton *ktRetBtn;
    QPushButton *sxConUpBtn;
    QPushButton *sxConDownBtn;
    QPushButton *sxConLeftBtn;
    QPushButton *sxConRightBtn;
    QPushButton *kt1OnBtn;
    QPushButton *kt1DownBtn;
    QPushButton *kt1OffBtn;
    QPushButton *kt2OnBtn;
    QPushButton *kt2DownBtn;
    QPushButton *kt2OffBtn;

    QCheckBox *RmbPwd;

    QHBoxLayout *ktOnLayout;
    QHBoxLayout *ktUpLayout;
    QHBoxLayout *sxLayout;
    QHBoxLayout *kt1Layout;
    QHBoxLayout *kt2Layout;

    QVBoxLayout *mainLayout;
    QVBoxLayout *ktClLayout;
    QVBoxLayout *ktLightLayout;
    QVBoxLayout *ktMainLayout;
    QVBoxLayout *sxMainLayout;

    QGridLayout *gridLayout;

    QPalette loginBgPalette;
    QPalette ktBgPalette;
};

#endif // EASTHOMEWIDGET_H
