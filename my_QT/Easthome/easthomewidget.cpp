 #include "easthomewidget.h"
#include "ui_easthomewidget.h"

#define MARGIN_LEFT 130
#define MARGIN_RIGHT 0
#define MARGIN_TOP 110
#define MARGIN_BUTTOM 50

#define NINE_MARGIN_LEFT 200
#define NINE_MARGIN_RIGHT 200
#define NINE_MARGIN_TOP 30
#define NINE_MARGIN_BUTTOM 150

#define KTBTN_W 180
#define KTBTN_H 68

#define KTBTN 23

EastHomeWidget::EastHomeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EastHomeWidget)
{
    ui->setupUi(this);

    //设置窗口最大化
    this->setWindowState(Qt::WindowMaximized);
    //设置隐藏标题栏
    this->setWindowFlag(Qt::FramelessWindowHint);
    //初始化登录界面
    initLogin();
    //初始化按钮
    initNineBtn();
    //初始化客厅界面
    ktInit();


    //跳转按钮连接槽函数
    connect(LoginBtn, SIGNAL(clicked(bool)), this, SLOT(loginBtnSlot()));
    //注销按钮连接槽函数
    connect(zhuxiaoPush, SIGNAL(clicked(bool)), this, SLOT(zhuxiaoPushSlot()));
    //客厅按钮连接槽函数
    connect(ktBtn, SIGNAL(clicked(bool)), this, SLOT(ktBtnSlot()));
    //连接返回按钮槽函数
    connect(ktRetBtn, SIGNAL(clicked(bool)), this, SLOT(ktRetBtnSlot()));
}

EastHomeWidget::~EastHomeWidget()
{
    delete ui;
}
//登录槽函数
void EastHomeWidget::loginBtnSlot()
{
    LoginBg->close();

    nineShow();
}
//注销槽函数
void EastHomeWidget::zhuxiaoPushSlot()
{
    nineClose();
    LoginBg->show();
}
//客厅按钮槽函数
void EastHomeWidget::ktBtnSlot()
{
    nineClose();
    this->setPalette(ktBgPalette);
    whiteBg->setParent(this);
    //显示白色北京
    whiteBg->show();
    //显示窗帘布局
    whiteBg->setLayout(ktClLayout);
    //显示灯光布局
    whiteBg->setLayout(ktLightLayout);
    //显示返回按钮
    ktRetBtn->show();
}

void EastHomeWidget::ktRetBtnSlot()
{
    this->setPalette(loginBgPalette);
    whiteBg->close();
    ktRetBtn->close();
    nineShow();
}
//初始化登录界面
void EastHomeWidget::initLogin()
{
    //设置window背景
    QPixmap BgPixmap (QString (":/new/prefix1/mypic/bg.png"));
    BgPixmap.scaled(this->width(), this->height());
    QBrush BgBrush (BgPixmap);
    loginBgPalette.setBrush(QPalette::Background, BgBrush);
    this->setPalette(loginBgPalette);

    //设置登录背景
    QPixmap pixmapHead (QString (":/new/prefix1/mypic/denglubeijing.png"));
    LoginBg = new QLabel(this);
    LoginBg->setPixmap(pixmapHead.scaled(600, 400));
    LoginBg->move(650, 300);

    //设置文本框背景
    QPixmap pixmapCntBg (QString(":/new/prefix1/mypic/yonghuming.png"));
    cntBg = new QLabel(LoginBg);
    cntBg->setPixmap(pixmapCntBg.scaled(364, 40));

    QPixmap pixmapPwdbg (QString (":/new/prefix1/mypic/mima.png"));
    pwdBg = new QLabel(LoginBg);
    pwdBg->setPixmap(pixmapPwdbg.scaled(364, 40));


    //设置账号密码文本框
    LoginEdit = new QLineEdit;
    PwdEdit = new QLineEdit;
    LoginEdit->setStyleSheet("border-radius: 3px;border: 1px solid black;border-top-width:0px;border-right-width:0px;border-bottom-width:0px;border-left-width:0px;");
    PwdEdit->setStyleSheet("border-radius: 3px;border: 1px solid black;border-top-width:0px;border-right-width:0px;border-bottom-width:0px;border-left-width:0px;");
    LoginEdit->setFixedSize(329, 35);
    PwdEdit->setFixedSize(329,35);
    LoginEdit->setPlaceholderText("用户名");
    PwdEdit->setPlaceholderText("密码");
    PwdEdit->setEchoMode(QLineEdit::Password);
    LoginEdit->setParent(cntBg);
    PwdEdit->setParent(pwdBg);
    LoginEdit->move(33, 20);
    PwdEdit->move(33, 20);

    //登录按钮
    LoginBtn = new QPushButton;
    LoginBtn->setParent(LoginBg);
    LoginBtn->setText("登录");
    LoginBtn->setStyleSheet("QPushButton{font-size: 20px;color:white; background-color:rgb(59,192,195);border-radius: 5px;}QPushButton::hover{background-color: #57cacf;}");
    LoginBtn->setFixedSize(364, 50);
    LoginBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    //记住密码
    RmbPwd = new QCheckBox(QString("记住密码"));

    //纵向布局
    mainLayout = new QVBoxLayout;
    mainLayout->addWidget(cntBg);
    mainLayout->addWidget(pwdBg);
    mainLayout->addWidget(LoginBtn);
    mainLayout->addWidget(RmbPwd);
    mainLayout->setContentsMargins(MARGIN_LEFT, MARGIN_TOP, MARGIN_RIGHT, MARGIN_BUTTOM);
    //mainLayout->setParent(LoginBg);
    LoginBg->setLayout(mainLayout);
}
//初始化九宫格界面
void EastHomeWidget::initNineBtn()
{

    //初始化按钮
    ktBtn = new QPushButton;
    ktBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/keting.png);");
    ktBtn->setFixedSize(497, 266);
    ktBtn->setFlat(true);
    ktBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    ws1Btn = new QPushButton;
    ws1Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/woshiyi.png);");
    ws1Btn->setFixedSize(497, 266);
    ws1Btn->setFlat(true);
    ws1Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    ckBtn = new QPushButton;
    ckBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/cheku1.png);");
    ckBtn->setFixedSize(497, 266);
    ckBtn->setFlat(true);
    ckBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    dxsBtn = new QPushButton;
    dxsBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/dixiashi.png);");
    dxsBtn->setFixedSize(497, 266);
    dxsBtn->setFlat(true);
    dxsBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    ws2Btn = new QPushButton;
    ws2Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/woshier.png);");
    ws2Btn->setFixedSize(497, 266);
    ws2Btn->setFlat(true);
    ws2Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    cfBtn = new QPushButton;
    cfBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/chufang.png);");
    cfBtn->setFixedSize(497, 266);
    cfBtn->setFlat(true);
    cfBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    wjBtn = new QPushButton;
    wjBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/waijing.png);");
    wjBtn->setFixedSize(497, 266);
    wjBtn->setFlat(true);
    wjBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    ws3Btn = new QPushButton;
    ws3Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/woshisan.png);");
    ws3Btn->setFixedSize(497, 266);
    ws3Btn->setFlat(true);
    ws3Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    wdmBtn = new QPushButton;
    wdmBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/waidamen.png);");
    wdmBtn->setFixedSize(497, 266);
    wdmBtn->setFlat(true);
    wdmBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    //网格布局
    gridLayout = new QGridLayout();
    gridLayout->addWidget(ktBtn, 0, 0);
    gridLayout->addWidget(ws1Btn, 0, 1);
    gridLayout->addWidget(ckBtn, 0, 2);
    gridLayout->addWidget(dxsBtn, 1, 0);
    gridLayout->addWidget(ws2Btn, 1, 1);
    gridLayout->addWidget(cfBtn, 1, 2);
    gridLayout->addWidget(wjBtn, 2, 0);
    gridLayout->addWidget(ws3Btn, 2, 1);
    gridLayout->addWidget(wdmBtn, 2, 2);
    gridLayout->setContentsMargins(NINE_MARGIN_LEFT, NINE_MARGIN_TOP, NINE_MARGIN_RIGHT, NINE_MARGIN_BUTTOM);


    zhuxiaoPush = new QPushButton;
    zhuxiaoPush->setText(QString ("注销"));
    zhuxiaoPush->setFlat(true);
    zhuxiaoPush->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    zhuxiaoPush->move(50, 900);
    zhuxiaoPush->setFixedSize(80, 50);
    zhuxiaoPush->setStyleSheet("QPushButton{color: #474a48;border: 0px;font-size: 20px;}QPushButton::hover{color:#000000 }");
}
//显示九宫格界面
void EastHomeWidget::nineShow()
{
    zhuxiaoPush->setParent(this);
    zhuxiaoPush->show();
    this->setLayout(gridLayout);
    ktBtn->show();
    ws1Btn->show();
    ckBtn->show();
    dxsBtn->show();
    ws2Btn->show();
    cfBtn->show();
    wjBtn->show();
    ws3Btn->show();
    wdmBtn->show();
}
//关闭九宫格界面
void EastHomeWidget::nineClose()
{
    ktBtn->close();
    ws1Btn->close();
    ckBtn->close();
    dxsBtn->close();
    ws2Btn->close();
    cfBtn->close();
    wjBtn->close();
    ws3Btn->close();
    wdmBtn->close();
    zhuxiaoPush->close();
}
//初始化客厅界面
void EastHomeWidget::ktInit()
{
    //kt bg
    QPixmap BgPixmap (QString (":/new/prefix1/mypic/bg.jpg"));
    BgPixmap.scaled(this->width(), this->height(),Qt::KeepAspectRatio, Qt::SmoothTransformation);
    QBrush BgBrush (BgPixmap);
    ktBgPalette.setBrush(QPalette::Background, BgBrush);

    //白色背景
    whiteBg = new QLabel;
    whiteBg->setStyleSheet("background-color: white;border-radius: 5px;");
    whiteBg->setFixedSize(1100, 550);
    whiteBg->move(410, 180);

    //按钮初始化
    clLOnBtn = new QPushButton;
    //clLOnBtn->setParent(whiteBg);
    clLOnBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/chuanglianzuokai.png);");
    clLOnBtn->setFixedSize(KTBTN_W, KTBTN_H);
    clLOnBtn->setFlat(true);
    clLOnBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    //clLOnBtn->show();

    clROffBtn = new QPushButton;
    //clROffBtn->setParent(whiteBg);
    clROffBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/chuanglianyouguan.png);");
    clROffBtn->setFixedSize(KTBTN_W, KTBTN_H);
    clROffBtn->setFlat(true);
    clROffBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    clLOffBtn = new QPushButton;
    //clLOffBtn->setParent(whiteBg);
    clLOffBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/chuanglianzuoguan.png);");
    clLOffBtn->setFixedSize(KTBTN_W, KTBTN_H);
    clLOffBtn->setFlat(true);
    clLOffBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    clROnBtn = new QPushButton;
   // clROnBtn->setParent(whiteBg);
    clROnBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/chuanglianyoukai.png);");
    clROnBtn->setFixedSize(KTBTN_W, KTBTN_H);
    clROnBtn->setFlat(true);
    clROnBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    bjqOnBtn = new QPushButton;
   // bjqOnBtn->setParent(whiteBg);
    bjqOnBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/dakaibaojingqi.png);");
    bjqOnBtn->setFixedSize(KTBTN_W, KTBTN_H);
    bjqOnBtn->setFlat(true);
    bjqOnBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    Light1Btn = new QPushButton;
   // Light1Btn->setParent(whiteBg);
    Light1Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/dengyi.png);");
    Light1Btn->setFixedSize(KTBTN_W, KTBTN_H);
    Light1Btn->setFlat(true);
    Light1Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    Light2Btn = new QPushButton;
    //Light2Btn->setParent(whiteBg);
    Light2Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/denger.png);");
    Light2Btn->setFixedSize(KTBTN_W, KTBTN_H);
    Light2Btn->setFlat(true);
    Light2Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    Light3Btn = new QPushButton;
   // Light3Btn->setParent(whiteBg);
    Light3Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/dengsan.png);");
    Light3Btn->setFixedSize(KTBTN_W, KTBTN_H);
    Light3Btn->setFlat(true);
    Light3Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    Light4Btn = new QPushButton;
   // Light4Btn->setParent(whiteBg);
    Light4Btn->setStyleSheet("background-image: url(:/new/prefix1/mypic/dengsi.png);");
    Light4Btn->setFixedSize(KTBTN_W, KTBTN_H);
    Light4Btn->setFlat(true);
    Light4Btn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    bjqOffBtn = new QPushButton;
    //bjqOffBtn->setParent(whiteBg);
    bjqOffBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/关闭报警器.png);");
    bjqOffBtn->setFixedSize(KTBTN_W, KTBTN_H);
    bjqOffBtn->setFlat(true);
    bjqOffBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    //窗帘按钮纵向布局
    ktClLayout = new QVBoxLayout;
    ktClLayout->addWidget(clROnBtn);
    ktClLayout->addWidget(clROffBtn);
    ktClLayout->addWidget(clLOnBtn);
    ktClLayout->addWidget(clLOffBtn);
    ktClLayout->addWidget(bjqOnBtn);

    //灯光按钮纵向布局
    ktLightLayout = new QVBoxLayout;
    ktLightLayout->addWidget(Light1Btn);
    ktLightLayout->addWidget(Light2Btn);
    ktLightLayout->addWidget(Light3Btn);
    ktLightLayout->addWidget(Light4Btn);
    ktLightLayout->addWidget(bjqOffBtn);


    //中心图片
    QPixmap pixmapHead (QString (":/new/prefix1/mypic/ceshineirong.jpg"));
    centerPic = new QLabel(whiteBg);
    centerPic->setPixmap(pixmapHead.scaled(700, 350));

    //顶部横向布局
    ktOnLayout = new QHBoxLayout;
    ktOnLayout->addLayout(ktClLayout);
    ktOnLayout->addWidget(centerPic);
    ktOnLayout->addLayout(ktLightLayout);
    //whiteBg->setLayout(ktOnLayout);

    QPixmap ktZuoPix (QString (":/new/prefix1/mypic/kongtiaoyi.png"));
    kongtiaozuoPic = new QLabel(whiteBg);
    kongtiaozuoPic->setPixmap(ktZuoPix.scaled(360, 142));

    QPixmap ktYouPix (QString (":/new/prefix1/mypic/kongtiaoer.png"));
    kongtiaoyouPic = new QLabel(whiteBg);
    kongtiaoyouPic->setPixmap(ktYouPix.scaled(360, 142));

    QPixmap jianKongPix (QString (":/new/prefix1/mypic/jiankonganniubeijing.png"));
    jianKongPic = new QLabel(whiteBg);
    jianKongPic->setPixmap(jianKongPix.scaled(142, 142));

    //底部横向布局
    ktUpLayout = new QHBoxLayout;
    ktUpLayout->addWidget(kongtiaozuoPic);
    ktUpLayout->addStretch();
    ktUpLayout->addWidget(jianKongPic);
    ktUpLayout->addStretch();
    ktUpLayout->addWidget(kongtiaoyouPic);

    //whiteBg->setLayout(ktUpLayout);

    //纵向布局
    ktMainLayout = new QVBoxLayout;
    ktMainLayout->addLayout(ktOnLayout);
    ktMainLayout->addLayout(ktUpLayout);
    whiteBg->setLayout(ktMainLayout);

    ktMainLayout->setContentsMargins(5, 5, 5, 5);

    //返回按钮
    ktRetBtn = new QPushButton(this);
    ktRetBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/back.png);border: 0px;");
    ktRetBtn->setFixedSize(78, 80);
    ktRetBtn->setFlat(true);
    ktRetBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    ktRetBtn->move(250, 800);

    ktRetBtn->close();

//    //控制摄像头旋转按钮
//    sxConUpBtn = new QPushButton;
//    sxConUpBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/xiangshang.png);");
//    sxConUpBtn->setFixedSize(22, 18);
//    //sxConUpBtn->setFlat(true);
//    sxConUpBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

//    sxConDownBtn = new QPushButton;
//    sxConDownBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/xiangxia.png);");


//    sxConLeftBtn = new QPushButton;
//    sxConLeftBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/xiangzuo.png);");


//    sxConRightBtn = new QPushButton;
//    sxConRightBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/xiangyou.png);");


//    //为摄像头控制设计布局
//    sxLayout = new QHBoxLayout;
//    sxLayout->addWidget(sxConLeftBtn);
//    sxLayout->addWidget(sxConRightBtn);

//    sxMainLayout = new QVBoxLayout;
//    sxMainLayout->addWidget(sxConUpBtn);
//    sxMainLayout->addLayout(sxLayout);
//    sxMainLayout->addWidget(sxConDownBtn);
//    jianKongPic->setLayout(sxMainLayout);

    //空调控制按钮
    kt1OnBtn = new QPushButton;
    kt1OnBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/shengwen.png);");
    kt1OnBtn->setFixedSize(KTBTN, KTBTN);
    kt1OnBtn->setFlat(true);
    kt1OnBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    kt1OffBtn = new QPushButton;
    kt1OffBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/off.png);");
    kt1OffBtn->setFixedSize(28, 28);
    kt1OffBtn->setFlat(true);
    kt1OffBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    kt1DownBtn = new QPushButton;
    kt1DownBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/jiangwen.png);");
    kt1DownBtn->setFixedSize(KTBTN, KTBTN);
    kt1DownBtn->setFlat(true);
    kt1DownBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    kt1Layout = new QHBoxLayout;
    kt1Layout->addWidget(kt1OnBtn);
    kt1Layout->addWidget(kt1OffBtn);
    kt1Layout->addWidget(kt1DownBtn);
    kongtiaozuoPic->setLayout(kt1Layout);

    kt1Layout->setContentsMargins(200, 100, 10, 30);

    kt2OnBtn = new QPushButton;
    kt2OnBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/shengwen.png);");
    kt2OnBtn->setFixedSize(KTBTN, KTBTN);
    kt2OnBtn->setFlat(true);
    kt2OnBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    kt2OffBtn = new QPushButton;
    kt2OffBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/off.png);");
    kt2OffBtn->setFixedSize(28, 28);
    kt2OffBtn->setFlat(true);
    kt2OffBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    kt2DownBtn = new QPushButton;
    kt2DownBtn->setStyleSheet("background-image: url(:/new/prefix1/mypic/jiangwen.png);");
    kt2DownBtn->setFixedSize(KTBTN, KTBTN);
    kt2DownBtn->setFlat(true);
    kt2DownBtn->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));

    kt2Layout = new QHBoxLayout;
    kt2Layout->addWidget(kt2OnBtn);
    kt2Layout->addWidget(kt2OffBtn);
    kt2Layout->addWidget(kt2DownBtn);
    kongtiaoyouPic->setLayout(kt2Layout);

    kt2Layout->setContentsMargins(10, 100, 200, 30);




}

