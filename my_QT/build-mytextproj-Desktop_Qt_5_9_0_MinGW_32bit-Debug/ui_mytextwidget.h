/********************************************************************************
** Form generated from reading UI file 'mytextwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYTEXTWIDGET_H
#define UI_MYTEXTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MytextWidget
{
public:

    void setupUi(QWidget *MytextWidget)
    {
        if (MytextWidget->objectName().isEmpty())
            MytextWidget->setObjectName(QStringLiteral("MytextWidget"));
        MytextWidget->resize(400, 300);

        retranslateUi(MytextWidget);

        QMetaObject::connectSlotsByName(MytextWidget);
    } // setupUi

    void retranslateUi(QWidget *MytextWidget)
    {
        MytextWidget->setWindowTitle(QApplication::translate("MytextWidget", "MytextWidget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MytextWidget: public Ui_MytextWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYTEXTWIDGET_H
